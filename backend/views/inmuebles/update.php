<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Inmuebles */
?>
<div class="inmuebles-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
