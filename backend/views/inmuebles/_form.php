<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Activo;
use app\models\Habitaciones;
use app\models\Ciudades;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Inmuebles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inmuebles-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'Ciudad')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Ciudades::find()->all(),'ciudad','ciudad'),
        'language' => 'en',
        'options' => ['placeholder' => 'Seleccione la ciudad'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>


    <?= $form->field($model, 'Direccion')->textInput(['rows' => 100]) ?>

    <?= $form->field($model, 'Tipo')->radioList(['Venta' => 'Venta', 'Arriendo' => 'Arriendo'])->label('Tipo de transacción'); ?>

    <?= $form->field($model, 'Habitaciones')->dropDownList(ArrayHelper::map(Habitaciones::find()->all(),'id','id'),['prompt'=>'Seleccione']) ?>

    <?= $form->field($model, 'Numparqueaderos')->textInput() ?>

    <?= $form->field($model, 'Antiguedad')->textInput(['rows' => 6]) ?>

    <?= $form->field($model, 'Area')->textInput(['rows' => 6]) ?>

    <?= $form->field($model, 'Baños')->textInput(['rows' => 6]) ?>

    <?= $form->field($model, 'Activo')->dropDownList(ArrayHelper::map(Activo::find()->all(),'estado','estado'),['prompt'=>'Seleccione']) ?>

  
    <?php if (!Yii::$app->request->isAjax){ ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>