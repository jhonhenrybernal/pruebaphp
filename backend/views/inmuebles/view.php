<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Inmuebles */
?>
<div class="inmuebles-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Ciudad:ntext',
            'Direccion:ntext',
            'Tipo:ntext',
            'Habitaciones',
            'Numparqueaderos',
            'Antiguedad:ntext',
            'Area:ntext',
            'Activo:ntext',
            'id',
            'Baños',
        ],
    ]) ?>

</div>
