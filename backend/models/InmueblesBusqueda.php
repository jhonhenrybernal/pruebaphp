<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inmuebles;

/**
 * InmueblesBusqueda represents the model behind the search form about `app\models\Inmuebles`.
 */
class InmueblesBusqueda extends Inmuebles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Ciudad', 'Direccion', 'Tipo', 'Antiguedad', 'Area', 'Activo'], 'safe'],
            [['Habitaciones', 'Numparqueaderos', 'id', 'Baños'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inmuebles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Habitaciones' => $this->Habitaciones,
            'Numparqueaderos' => $this->Numparqueaderos,
            'id' => $this->id,
            'Baños' => $this->Baños,
        ]);

        $query->andFilterWhere(['like', 'Ciudad', $this->Ciudad])
            ->andFilterWhere(['like', 'Direccion', $this->Direccion])
            ->andFilterWhere(['like', 'Tipo', $this->Tipo])
            ->andFilterWhere(['like', 'Antiguedad', $this->Antiguedad])
            ->andFilterWhere(['like', 'Area', $this->Area])
            ->andFilterWhere(['like', 'Activo', $this->Activo]);

        return $dataProvider;
    }
}
