<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activo".
 *
 * @property int $id
 * @property string $estado
 * @property int $valor
 */
class Activo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'activo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'estado', 'valor'], 'required'],
            [['id', 'valor'], 'integer'],
            [['estado'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estado' => 'Estado',
            'valor' => 'Valor',
        ];
    }
}
