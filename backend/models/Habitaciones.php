<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habitaciones".
 *
 * @property int $id
 * @property string $cantidad
 * @property int $cant_numero
 */
class Habitaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'habitaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cantidad', 'cant_numero'], 'required'],
            [['id', 'cant_numero'], 'integer'],
            [['cantidad'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cantidad' => 'Cantidad',
            'cant_numero' => 'Cant Numero',
        ];
    }
}
