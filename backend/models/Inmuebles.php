<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inmuebles".
 *
 * @property string $Ciudad
 * @property string $Direccion
 * @property string $Tipo
 * @property int $Habitaciones
 * @property int $Numparqueaderos
 * @property string $Antiguedad
 * @property string $Area
 * @property string $Activo
 * @property int $id
 * @property int $Baños
 */
class Inmuebles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inmuebles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Ciudad', 'Direccion', 'Tipo', 'Antiguedad', 'Area', 'Activo'], 'string'],
            [['Habitaciones', 'Numparqueaderos', 'Baños'], 'integer'],
            [['Baños'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Ciudad' => 'Ciudad',
            'Direccion' => 'Direccion',
            'Tipo' => 'Tipo de transacción',
            'Habitaciones' => 'Numero de Habitaciones',
            'Numparqueaderos' => 'Numero de parqueaderos',
            'Antiguedad' => 'Antiguedad',
            'Area' => 'Area',
            'Activo' => 'Activo',
            'id' => 'ID',
            'Baños' => 'Numero de  Baños',
        ];
    }
}
